X=transpose(table2array(ibovtrainingxt(:,10));
scale=1000;
m=1;
segments=floor(length(X)/scale);
for v=1:segments
   Idx_start=((v-1)*scale)+1;
   Idx_stop=v*scale;
   Index{v}=Idx_start:Idx_stop;
   X_Idx=X(Index{v});
   C=polyfit(Index{v},X(Index{v}),m);
   fit{v}=polyval(C,Index{v});
   RMS{1}(v)=sqrt(mean((X_Idx-fit{v}).^2));
end
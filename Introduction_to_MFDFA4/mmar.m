% Simulates a Multifractal Model of Asset Return using a multiplicative 
% lognormal cascade
%
% See the following papaer
% A Multifractal Model of Asset Returns by B Mandelbrot - 1997 
%
% input:    b         Number of subdivision (i.e. 2 for binomial model)
%           k         Number of iterations, i.e. the MMAR is of length b^k
%           H         Hurst exponent of the fractal brownian motion
%           ln_mu     The mean value of the lognormal distribution
%           ln_sigma  The standard deviation of the lognormal distribution
%           
% output:   MMAR      The simulated log return
%
% example:  x = mmar(2, 10, 0.55, 0, 0.3);
%
% Author:   Christian Wengert
function MMAR = mmar(b, k, H, ln_mu, ln_sigma)
%Global parameters 
T = b^k;
%Constants (initial weight)
M0 = 1.0;
%Fractal Brownian motion
B = ffGn(T, 1.0 + H);
%Fractal time
[V, cdf_V] = lognormal_cascade(b, k, M0, ln_mu, ln_sigma);
%Normalize the cdf
cdf_V = cdf_V/cdf_V(end);
%allocate
MMAR = zeros(1,T);
%Compound
for i=1:T
  %Get fractal time
  index = cdf_V(i)*T;
  lower = floor(index);
  if(lower<1), lower = 1; end
  upper = ceil(index);
  if(upper>T), upper = T; end
  
  %Interpolate for compounding  
  w2    = cdf_V(i);
  w1    = 1.0 - cdf_V(i);
  %Compound
  MMAR(i) = w1*B(lower)+w2*B(upper);
end
%show results
figure,clf,
subplot(2,1,1)
hold on
plot(MMAR)
plot(B,'r')
legend('MMAR', 'fractional Brownian motion')
hold off
subplot(2,1,2)
plot(cdf_V,'g')
legend('CDF log normal multiplicative cascade')
hold off

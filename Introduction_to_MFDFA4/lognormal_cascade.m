% lognormal cascade measure
%
% Author:   Christian Wengert
%           
% Computes the Random (lognormal) Multifractal Measure iteratively.
%
% Input:    b         The number of intervals
%           k         The number of iterations (or stages)
%           v         The start value (or area)
%           ln_lambda The lognormal param 1
%           ln_theta  The lognormal param 2
%
% Output:   v   A row vector containing the weights after the last iteration
function [v, cdf_v] = lognormal_cascade(b, k, v, ln_lambda, ln_theta)
if(nargin~=5)
  error('Please specify all parameters')
end
%Decrement iterator
k = k-1;
%Draw new weights from a lognormal distribution
M=lognrnd (ln_lambda, ln_theta, 1, b);
%Stop condition
if(k>=0)
  %Empty
  d = [];
 
  %expansion
  for i=1:b
    d = [d lognormal_cascade(b, k, M(i)*v, ln_lambda, ln_theta)];
  end
  v = d;  
end
%Create the cumsum for the cdfif(nargout>1)
cdf_v = cumsum(v);
